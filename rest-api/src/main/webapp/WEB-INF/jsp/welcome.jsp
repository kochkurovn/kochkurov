<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="./component/header.jsp" />
    <form:form method="POST" action="/spring-mvc-xml/addEmployee" modelAttribute="userLoginModel">
        <div class="form-group row">
            <h1 class="text-center">Welcome</h1>
            <form:label path="name" for="name">Name</form:label>
            <form:input cssClass="form-control" path="name" type="text" required="required"/>
            <form:label path="password" for="password">Password</form:label>
            <form:input cssClass="form-control" path="password" type="password" required="required"/>
            <input type="submit" class="btn btn-primary" value="Send" />
        </div>
    </form:form>
<jsp:include page="./component/footer.jsp" />



