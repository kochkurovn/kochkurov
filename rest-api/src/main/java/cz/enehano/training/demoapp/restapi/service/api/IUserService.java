package cz.enehano.training.demoapp.restapi.service.api;

import cz.enehano.training.demoapp.restapi.entity.Users;

import java.util.List;

public interface IUserService {

	List<Users> getAllUsers();

	Users getUser(Long id);

	Users createUser(Users users);

	void deleteUser(Long id);
}
