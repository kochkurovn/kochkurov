package cz.enehano.training.demoapp.restapi.controller;

import cz.enehano.training.demoapp.restapi.model.UserLoginModel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WelcomeController {

	@RequestMapping("/")
	public ModelAndView welcome() {
		ModelAndView modelAndView = new ModelAndView("welcome", "userLoginModel", new UserLoginModel());
		return modelAndView;
	}

}
