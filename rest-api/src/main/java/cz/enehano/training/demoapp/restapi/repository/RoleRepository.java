package cz.enehano.training.demoapp.restapi.repository;

import cz.enehano.training.demoapp.restapi.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
