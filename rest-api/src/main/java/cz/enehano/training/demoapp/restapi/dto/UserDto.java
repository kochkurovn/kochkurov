package cz.enehano.training.demoapp.restapi.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

@Data
@EqualsAndHashCode
@ToString
public class UserDto implements Serializable {
    
    private static final long serialVersionUID = -778816459886472677L;
    private String name;
    private String surname;
    private String address;
    private String email;
    private String phone;
    private String password;
}
