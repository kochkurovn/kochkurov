package cz.enehano.training.demoapp.restapi.service;

import cz.enehano.training.demoapp.restapi.entity.Users;
import cz.enehano.training.demoapp.restapi.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailService implements UserDetailsService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Users> optionalUsers = userRepository.findByName(username);
		optionalUsers.orElseThrow(() ->  new UsernameNotFoundException("Username not found"));
		return optionalUsers.map(CustomUsersDetails::new).get();
	}
}
