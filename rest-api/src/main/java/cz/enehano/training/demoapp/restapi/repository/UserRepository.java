package cz.enehano.training.demoapp.restapi.repository;

import cz.enehano.training.demoapp.restapi.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface UserRepository extends JpaRepository<Users, Long> {
	Optional<Users> findByName(String username);
}
