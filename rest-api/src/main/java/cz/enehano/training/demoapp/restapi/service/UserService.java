package cz.enehano.training.demoapp.restapi.service;

import cz.enehano.training.demoapp.restapi.entity.Users;
import cz.enehano.training.demoapp.restapi.repository.UserRepository;
import cz.enehano.training.demoapp.restapi.service.api.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<Users> getAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public Users getUser(Long id) {
		return userRepository.getOne(id);
	}

	@Override
	public Users createUser(Users users) {
		return userRepository.saveAndFlush(users);
	}

	@Override
	public void deleteUser(Long id) {
		userRepository.deleteById(id);
	}
}
