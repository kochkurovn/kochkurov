package cz.enehano.training.demoapp.restapi.controller;

import cz.enehano.training.demoapp.restapi.dto.UserDto;
import cz.enehano.training.demoapp.restapi.provider.UserProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/secured/user")
public class UserController {

	@Autowired
	private UserProvider userProvider;
	
	@PreAuthorize("hasAnyRole('CREATE_USERS')")
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	@ResponseBody
	public List<UserDto> getAllUsers() {
		return userProvider.getAllUsers();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public UserDto getUser(@PathVariable("id") Long id) {
		return userProvider.getUser(id);
	}

	@PreAuthorize("hasAnyRole('CREATE_USERS')")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public void createUser(@RequestBody UserDto dto) {
		userProvider.createUser(dto);
	}
	
	@PreAuthorize("hasAnyRole('UPDATE_USERS')")
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	@ResponseBody
	public UserDto updateUser(@RequestBody UserDto dto, @PathVariable("id") Long id) {
		return userProvider.updateUser(dto, id);
	}
	
	@PreAuthorize("hasAnyRole('DELETE_USERS')")
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteUser(@PathVariable("id") Long id) {
		userProvider.deleteUser(id);
	}
}
