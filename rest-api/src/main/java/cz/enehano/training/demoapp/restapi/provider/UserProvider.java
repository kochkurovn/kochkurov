package cz.enehano.training.demoapp.restapi.provider;

import cz.enehano.training.demoapp.restapi.dto.UserDto;
import cz.enehano.training.demoapp.restapi.entity.Users;
import cz.enehano.training.demoapp.restapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserProvider {

	@Autowired
	private UserService userService;

	public List<UserDto> getAllUsers() {
		List<UserDto> userDtoList = new ArrayList<>();
		for (Users item : userService.getAllUsers()) {
			userDtoList.add(getUserDto(item));
		}
		return userDtoList;
	}

	public void createUser(UserDto userDto) {
		userService.createUser(getUsers(userDto, new Users()));
	}

	public UserDto getUser(Long id) {
		return getUserDto(userService.getUser(id));
	}

	public UserDto updateUser(UserDto userDto, Long id) {
		getUsers(userDto, userService.getUser(id));
		return userDto;
	}
	
	public void deleteUser(Long id) {
		userService.deleteUser(id);
	}
	
	private UserDto getUserDto(Users user) {
		UserDto userDto = new UserDto();
		userDto.setAddress(user.getAddress());
		userDto.setEmail(user.getEmail());
		userDto.setName(user.getName());
		userDto.setPhone(user.getPhone());
		userDto.setSurname(user.getSurname());
		return userDto;
	}
	
	private Users getUsers(UserDto userDto, Users users) {
		users.setAddress(userDto.getAddress());
		users.setEmail(userDto.getEmail());
		users.setName(userDto.getName());
		users.setPhone(userDto.getPhone());
		users.setSurname(userDto.getSurname());
		users.setPassword(userDto.getPassword());
		return users;
	}
}
