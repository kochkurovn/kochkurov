package cz.enehano.training.demoapp.restapi.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
public class UserLoginModel {
	private String name;
	private String password;
}
